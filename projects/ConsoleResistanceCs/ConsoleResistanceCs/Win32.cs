﻿using System;
using System.Runtime.InteropServices;

namespace ConsoleResistanceCs
{
    class Win32
    {

        public const uint WM_SETREDRAW = 0x000B;

        /// <summary>
        /// https://social.msdn.microsoft.com/Forums/ja-JP/e2d37ee0-ab0c-4c5a-b336-9cb39b451b91/textbox-multiline-limit?forum=winforms
        /// </summary>
        public const uint WM_USER = 0x400;
        public const uint EM_GETEVENTMASK = (WM_USER + 59);
        public const uint EM_SETEVENTMASK = (WM_USER + 69);

        /// <summary>
        /// http://www.pinvoke.net/default.aspx/user32/SendMessage.html
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="Msg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }
}
