﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ConsoleResistanceCs {
    public partial class ConsoleControl : UserControl {

        #region [constants]
        const int MAX_LINE = 1000;
        #endregion

        #region [member]
        private StringBuilder buffer = new StringBuilder();
        private object lockObj = new object();
        private List<string> lines = new List<string>();
        private int maxLines = MAX_LINE;
        private int limitLines = MAX_LINE * 2;
        #endregion

        #region [property]
        public Action<string> AppendLine { get; private set; }
        public int MaxLines {
            get {
                return this.maxLines;
            }
            set {
                this.maxLines = value;
            }
        }
        public int LmitLines {
            get {
                return this.limitLines;
            }
            set {
                this.limitLines = value;
            }
        }

        public TextBox TextBox {
            get {
                return this.textBox;
            }
        }
        #endregion

        #region [constructor]
        public ConsoleControl() {
            InitializeComponent();
            SuspendLayout();
            DoubleBuffered = true;

            AppendLine = AppendLineLimitTrim;

            ResumeLayout(false);
        }
        #endregion

        #region [add line method]
        private void AppendLineSimple(string str) {
            StringBuilder sb = new StringBuilder();

            using (StringReader r = new StringReader(str)) {
                string line;
                while ((line = r.ReadLine()) != null) {
                    lines.Add(line);
                }
            }
            while (lines.Count > MaxLines) {
                lines.RemoveAt(0);
            }
            foreach (string line in lines) {
                sb.AppendLine(line);
            }
            textBox.Text = sb.ToString();
            textBox.SelectionStart = sb.Length;
            textBox.SelectionLength = 0;
            textBox.ScrollToCaret();
        }

        private void AppendLineNoRedraw(string str) {
            IntPtr eventMask = IntPtr.Zero;
            try {
                Win32.SendMessage(textBox.Handle, Win32.WM_SETREDRAW, IntPtr.Zero, IntPtr.Zero);
                AppendLineSimple(str);
                eventMask = Win32.SendMessage(textBox.Handle, Win32.EM_GETEVENTMASK, IntPtr.Zero, IntPtr.Zero);
            } finally {
                Win32.SendMessage(textBox.Handle, Win32.EM_SETEVENTMASK, IntPtr.Zero, eventMask);
                Win32.SendMessage(textBox.Handle, Win32.WM_SETREDRAW, (IntPtr)1, IntPtr.Zero);
                textBox.ScrollToCaret();
            }
        }

        private void AppendLineExAsync(string str) {
            BackgroundWorker woker = new BackgroundWorker();
            {
                woker.WorkerSupportsCancellation = false;
                woker.WorkerReportsProgress = true;
                woker.ProgressChanged += new ProgressChangedEventHandler(delegate(object sender, ProgressChangedEventArgs e) {
                    if (e.ProgressPercentage == 0) {
                        Win32.SendMessage(base.Handle, Win32.WM_SETREDRAW, (IntPtr)0, (IntPtr)0);
                    } else {
                        IntPtr eventMask = IntPtr.Zero;
                        eventMask = Win32.SendMessage(textBox.Handle, Win32.EM_GETEVENTMASK, IntPtr.Zero, IntPtr.Zero);
                        Win32.SendMessage(textBox.Handle, Win32.EM_SETEVENTMASK, IntPtr.Zero, eventMask);
                        Win32.SendMessage(base.Handle, Win32.WM_SETREDRAW, (IntPtr)1, (IntPtr)0);
                        textBox.ScrollToCaret();
                        textBox.Invalidate();
                        textBox.Update();
                    }
                });
                woker.DoWork += new DoWorkEventHandler(delegate(object sender, DoWorkEventArgs e) {
                    lock (lockObj) {
                        woker.ReportProgress(0);
                        try {
                            Action action = delegate() {
                                AppendLineSimple(str);
                            };
                            Invoke(action);
                        } finally {
                            woker.ReportProgress(100);
                        }
                    }
                });

                woker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(delegate(object sender, RunWorkerCompletedEventArgs e) {
                });
            }
            woker.RunWorkerAsync();
        }

        private void AppendLineLimitTrim(string str) {
            int max = Math.Min(int.MaxValue, Math.Max(this.maxLines, this.limitLines));
            List<string> appendLineList = new List<string>();
            using (StringReader r = new StringReader(str)) {
                string line;
                while ((line = r.ReadLine()) != null) {
                    appendLineList.Add(line);
                }
            }
            lines.AddRange(appendLineList);
            if (lines.Count < max) {
                StringBuilder sb = new StringBuilder();
                foreach (string line in appendLineList) {
                    sb.AppendLine(line);
                }
                textBox.AppendText(sb.ToString());
            } else {
                IntPtr eventMask = IntPtr.Zero;
                Win32.SendMessage(textBox.Handle, Win32.WM_SETREDRAW, IntPtr.Zero, IntPtr.Zero);
                StringBuilder sb = new StringBuilder();
                while (lines.Count > MaxLines) {
                    lines.RemoveAt(0);
                }
                foreach (string line in lines) {
                    sb.AppendLine(line);
                }
                textBox.Text = sb.ToString();
                textBox.SelectionStart = sb.Length;
                textBox.SelectionLength = 0;
                eventMask = Win32.SendMessage(textBox.Handle, Win32.EM_GETEVENTMASK, IntPtr.Zero, IntPtr.Zero);
                Win32.SendMessage(textBox.Handle, Win32.EM_SETEVENTMASK, IntPtr.Zero, eventMask);
                Win32.SendMessage(textBox.Handle, Win32.WM_SETREDRAW, (IntPtr)1, IntPtr.Zero);
                textBox.ScrollToCaret();
            }
        }
        #endregion
    }
}
