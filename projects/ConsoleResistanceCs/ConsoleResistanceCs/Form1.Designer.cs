﻿namespace ConsoleResistanceCs {
  partial class Form1 {
    /// <summary>
    /// 必要なデザイナー変数です。
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 使用中のリソースをすべてクリーンアップします。
    /// </summary>
    /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
    protected override void Dispose(bool disposing) {
      if(disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows フォーム デザイナーで生成されたコード

    /// <summary>
    /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
    /// コード エディターで変更しないでください。
    /// </summary>
    private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.textBoxInputBox = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.consoleControl = new ConsoleResistanceCs.ConsoleControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.topMostToolStripMenuItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.versionToolStripMenuItemMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripTray = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showToolStripMenuItemTray = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.closeToolStripMenuItemNotify = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStripTray.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxInputBox
            // 
            this.textBoxInputBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxInputBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxInputBox.Location = new System.Drawing.Point(0, 447);
            this.textBoxInputBox.Name = "textBoxInputBox";
            this.textBoxInputBox.Size = new System.Drawing.Size(746, 19);
            this.textBoxInputBox.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.consoleControl);
            this.panel1.Controls.Add(this.textBoxInputBox);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(746, 466);
            this.panel1.TabIndex = 6;
            // 
            // consoleControl
            // 
            this.consoleControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consoleControl.LmitLines = 2000;
            this.consoleControl.Location = new System.Drawing.Point(0, 26);
            this.consoleControl.MaxLines = 1000;
            this.consoleControl.Name = "consoleControl";
            this.consoleControl.Size = new System.Drawing.Size(746, 421);
            this.consoleControl.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItemMenu,
            this.windowToolStripMenuItemMenu,
            this.helpToolStripMenuItemMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(746, 26);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItemMenu
            // 
            this.fileToolStripMenuItemMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItemMenu});
            this.fileToolStripMenuItemMenu.Name = "fileToolStripMenuItemMenu";
            this.fileToolStripMenuItemMenu.Size = new System.Drawing.Size(54, 22);
            this.fileToolStripMenuItemMenu.Text = "file(&F)";
            // 
            // closeToolStripMenuItemMenu
            // 
            this.closeToolStripMenuItemMenu.Name = "closeToolStripMenuItemMenu";
            this.closeToolStripMenuItemMenu.Size = new System.Drawing.Size(152, 22);
            this.closeToolStripMenuItemMenu.Text = "close(&C)";
            // 
            // windowToolStripMenuItemMenu
            // 
            this.windowToolStripMenuItemMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.topMostToolStripMenuItemMenu});
            this.windowToolStripMenuItemMenu.Name = "windowToolStripMenuItemMenu";
            this.windowToolStripMenuItemMenu.Size = new System.Drawing.Size(86, 22);
            this.windowToolStripMenuItemMenu.Text = "window(&W)";
            // 
            // topMostToolStripMenuItemMenu
            // 
            this.topMostToolStripMenuItemMenu.Name = "topMostToolStripMenuItemMenu";
            this.topMostToolStripMenuItemMenu.Size = new System.Drawing.Size(152, 22);
            this.topMostToolStripMenuItemMenu.Text = "top most(&T)";
            // 
            // helpToolStripMenuItemMenu
            // 
            this.helpToolStripMenuItemMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionToolStripMenuItemMenu});
            this.helpToolStripMenuItemMenu.Name = "helpToolStripMenuItemMenu";
            this.helpToolStripMenuItemMenu.Size = new System.Drawing.Size(63, 22);
            this.helpToolStripMenuItemMenu.Text = "help(&H)";
            // 
            // versionToolStripMenuItemMenu
            // 
            this.versionToolStripMenuItemMenu.Name = "versionToolStripMenuItemMenu";
            this.versionToolStripMenuItemMenu.Size = new System.Drawing.Size(136, 22);
            this.versionToolStripMenuItemMenu.Text = "version(&V)";
            // 
            // notifyIcon
            // 
            this.notifyIcon.ContextMenuStrip = this.contextMenuStripTray;
            this.notifyIcon.Text = "ConsoleRegistanceCs";
            this.notifyIcon.Visible = true;
            // 
            // contextMenuStripTray
            // 
            this.contextMenuStripTray.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItemTray,
            this.toolStripSeparator1,
            this.closeToolStripMenuItemNotify});
            this.contextMenuStripTray.Name = "contextMenuStripTray";
            this.contextMenuStripTray.Size = new System.Drawing.Size(125, 54);
            // 
            // showToolStripMenuItemTray
            // 
            this.showToolStripMenuItemTray.Name = "showToolStripMenuItemTray";
            this.showToolStripMenuItemTray.Size = new System.Drawing.Size(124, 22);
            this.showToolStripMenuItemTray.Text = "show(&S)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // closeToolStripMenuItemNotify
            // 
            this.closeToolStripMenuItemNotify.Name = "closeToolStripMenuItemNotify";
            this.closeToolStripMenuItemNotify.Size = new System.Drawing.Size(124, 22);
            this.closeToolStripMenuItemNotify.Text = "close(&C)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 466);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "ConsoleResistanceCs";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStripTray.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TextBox textBoxInputBox;
    private System.Windows.Forms.Panel panel1;
    private ConsoleControl consoleControl;
    private System.Windows.Forms.NotifyIcon notifyIcon;
    private System.Windows.Forms.ContextMenuStrip contextMenuStripTray;
    private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItemTray;
    private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItemNotify;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItemMenu;
    private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItemMenu;
    private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItemMenu;
    private System.Windows.Forms.ToolStripMenuItem topMostToolStripMenuItemMenu;
    private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItemMenu;
    private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItemMenu;


  }
}

