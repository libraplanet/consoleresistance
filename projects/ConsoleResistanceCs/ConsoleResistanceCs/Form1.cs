﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;

namespace ConsoleResistanceCs {
    public partial class Form1 : Form {
        Setting setting;
        Process proc;
        event DataReceivedEventHandler dataReceivedEvent = null;

        string AutoRunPath {
            get {
                string dirPath = Path.GetDirectoryName(Application.ExecutablePath);
                string exename = Path.GetFileNameWithoutExtension(Application.ExecutablePath);
                return Path.Combine(dirPath, string.Format("{0}{1}", exename, ".autorun"));
            }
        }

        string SettingPath {
            get {
                string dirPath = Path.GetDirectoryName(Application.ExecutablePath);
                string exename = Path.GetFileNameWithoutExtension(Application.ExecutablePath);
                return Path.Combine(dirPath, string.Format("{0}{1}", exename, ".setting"));
            }
        }


        /// <summary>
        /// 
        /// http://qiita.com/yohhoy/items/b6e32e17c9d568f927d8
        /// </summary>
        /// <param name="pid"></param>
        private static void KillProcessTree(int pid) {
#if false
            string taskkill = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "taskkill.exe");
            using (var procKiller = new System.Diagnostics.Process()) {
                procKiller.StartInfo.FileName = taskkill;
                procKiller.StartInfo.Arguments = string.Format("/PID {0} /T /F", pid);
                procKiller.StartInfo.CreateNoWindow = true;
                procKiller.StartInfo.UseShellExecute = false;
                procKiller.Start();
                procKiller.WaitForExit();
            }
#else
            KillProcessAndChildren(pid);
#endif
        }
        /// <summary>
        /// http://stackoverflow.com/questions/23845395/in-c-how-to-kill-a-process-tree-reliably
        /// </summary>
        /// <param name="pid"></param>
        private static void KillProcessAndChildren(int pid) {
            string query = string.Format("Select * From Win32_Process Where ParentProcessID={0}", pid);
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc) {
                KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
            }
            try {
                Process proc = Process.GetProcessById(pid);
                proc.Kill();
            } catch (ArgumentException) {
                // Process already exited.
            }
        }
        private static void SetConsoleColor(TextBox textbox, Setting settings) {
            textbox.Font = settings.Console.Font;
            textbox.ForeColor = settings.Console.ForeColor;
            textbox.BackColor = settings.Console.BackColor;
        }
        private static void SetInputBoxColor(TextBox textbox, Setting settings) {
            textbox.Font = settings.Input.Font;
            textbox.ForeColor = settings.Input.ForeColor;
            textbox.BackColor = settings.Input.BackColor;
        }

        public Form1() {
            InitializeComponent();
            textBoxInputBox.Focus();
            FormWindowState windowState = FormWindowState.Normal;
            Action<bool> setVisible= delegate (bool b){
                if (b) {
                    if (!this.Visible) {
                        this.ShowInTaskbar = true;
                        this.Visible = true;
                        this.WindowState = windowState;
                        this.Show();
                    }
                    this.Activate();
                } else {
                    this.WindowState = FormWindowState.Minimized;
                    this.ShowInTaskbar = false;
                    this.Hide();
                }
            };
            EventHandler stateLogerEvent = new EventHandler(delegate(object sender, EventArgs e) {
                if (this.WindowState != FormWindowState.Minimized) {
                    windowState = this.WindowState;
# if true
                }
#else
                } else {
                    if (this.Visible) {
                        this.ShowInTaskbar = false;
                        this.Hide();
                    }
                }
#endif
            });
            FormClosingEventHandler formClosingEvent = new FormClosingEventHandler(delegate(object sender, FormClosingEventArgs e) {
                if (e.CloseReason != CloseReason.WindowsShutDown) {
                    setVisible(false);
                    e.Cancel = true;
                }
            });
            //form event
            {
                Load += stateLogerEvent;
                SizeChanged += stateLogerEvent;
                LocationChanged += stateLogerEvent;
                FormClosing += formClosingEvent;
            }

            //tray event
            {
                showToolStripMenuItemTray.Click += new EventHandler(delegate(object sender, EventArgs e) {
                    setVisible(true);
                });
                closeToolStripMenuItemNotify.Click += new EventHandler(delegate(object sender, EventArgs e) {
                    FormClosing -= formClosingEvent;
                    this.Close();
                });
                notifyIcon.MouseDoubleClick += new MouseEventHandler(delegate(object sender, MouseEventArgs e) {
                    setVisible(!this.Visible);
                });
            }
            //menu event
            {
                closeToolStripMenuItemMenu.Click += new EventHandler(delegate(object sender, EventArgs e) {
                    FormClosing -= formClosingEvent;
                    this.Close();
                });
                topMostToolStripMenuItemMenu.Click += new EventHandler(delegate(object sender, EventArgs e) {
                    TopMost = !TopMost;
                    topMostToolStripMenuItemMenu.Checked = TopMost;
                });
                versionToolStripMenuItemMenu.Click += new EventHandler(delegate(object sender, EventArgs e) {
                    string s;
                    using (StringWriter w = new StringWriter()) {
                        w.WriteLine(string.Format("{0} {1}", new object[] { this.Text, "v0.0.0.0.0.0.0.0.0.3" }));
                        s = w.ToString();
                    }
                    MessageBox.Show(s, "version", MessageBoxButtons.OK, MessageBoxIcon.Information);
                });
            }
            //inputbox event
            {
                textBoxInputBox.KeyPress += new KeyPressEventHandler(delegate(object sender, KeyPressEventArgs e) {
                    if (e.KeyChar == (int)Keys.Enter) {
                        e.Handled = true;
                    } else if (e.KeyChar == (int)Keys.Escape) {
                        e.Handled = true;
                    }
                });
                textBoxInputBox.KeyDown += new KeyEventHandler(delegate(object sender, KeyEventArgs e) {
                    if (e.KeyCode == Keys.Enter) {
                        string cmd = textBoxInputBox.Text;
                        textBoxInputBox.Text = "";
                        proc.StandardInput.WriteLine(cmd);
                        e.Handled = true;
                    }
                });
            }
            //standard I/O event
            {
                dataReceivedEvent += new DataReceivedEventHandler(delegate(object sender, DataReceivedEventArgs e) {
                    AddConsoleLine(e.Data);
                });
            }
        }

        private void InitCmdProc() {
            Process proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";
            proc.StartInfo.Arguments = "";
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.OutputDataReceived += this.dataReceived;
            proc.ErrorDataReceived += this.dataReceived;
            proc.Start();
            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();
            this.proc = proc;
        }

        private void LoadSettings()
        {
            Setting setting;
            try {
                setting = Setting.Load(SettingPath);
            } catch {
                setting = new Setting();
            }

            if (!string.Equals(setting.Caption, Setting.DEFAULT_CAPTION)) {
                this.Text = setting.Caption;
            }
            if (!string.Equals(setting.IconFile, Setting.DEFAULT_ICON)) {
                string path = new Uri(Path.GetFullPath(setting.IconFile)).AbsolutePath;
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                    this.Icon = new Icon(stream);
                }
                this.Text = setting.Caption;
            }
            this.setting = setting;
        }

        private void LoadAutoRun() {
            try {
                AutoRun autorun = AutoRun.Load(AutoRunPath);
                try {
                    String cmd;
                    StringBuilder sb = new StringBuilder();
                    string exeFilePath = autorun.ExecuteFile;
                    string exeDirPath = Path.GetFullPath(autorun.WorkingDir);
                    sb.Append(string.Format(" \"{0}\"", exeFilePath));
                    if (autorun.Arguments != null) {
                        foreach (string arg in autorun.Arguments) {
                            sb.Append(string.Format(" \"{0}\"", arg));
                        }
                    }
                    cmd = string.Format("(cd /d \"{0}\" & {1})", new object[] { exeDirPath, sb.ToString() });
                    proc.StandardInput.WriteLine(cmd);
                } catch (Exception e2) {
                    AddConsoleLine("Autorun失敗");
                    AddConsoleLine(e2.ToString());
                }
            } catch {
                AddConsoleLine("Autorunなし");
#if DEBUG
                try {
                    new AutoRun().Save(AutoRunPath);
                    AddConsoleLine("Autorun自動生成成功");
                } catch (Exception e2) {
                    AddConsoleLine("Autorun自動生成失敗");
                    AddConsoleLine(e2.ToString());
                }

#endif
            }
        }

        private void dataReceived(object sender, DataReceivedEventArgs e) {
            Invoke(dataReceivedEvent, new object[] { sender, e });
        }

        private void AddConsoleLine(string str) {
            consoleControl.AppendLine(str);
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            try {
                InitCmdProc();

                LoadSettings();
                SetConsoleColor(this.consoleControl.TextBox, setting);
                SetConsoleColor(this.textBoxInputBox, setting);
#if true
                LoadAutoRun();
#endif
            } finally {
                notifyIcon.Icon = this.Icon;
                notifyIcon.Text = this.Text;
            }
        }

        protected override void OnClosed(EventArgs e) {
            if (proc != null) {
                proc.OutputDataReceived -= this.dataReceived;
                proc.ErrorDataReceived -= this.dataReceived;
                KillProcessTree(proc.Id);
                proc.Close();
                proc.Dispose();
                proc = null;
            }
            base.OnClosed(e);
            try {
                setting.Save(SettingPath);
#if DEBUG
            } catch (Exception e2) {
                MessageBox.Show(e2.ToString(), "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
#else
            } catch {
            }
#endif
        }
    }
}
