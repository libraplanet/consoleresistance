﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class AutoRun : SerializeBase<AutoRun> {
    public string ExecuteFile { get; set; }
    public string WorkingDir { get; set; }
    public string[] Arguments { get; set; }

    public AutoRun() {
#if DEBUG
        ExecuteFile = @"t.bat";
        WorkingDir = @"..\..\..\..\test";
        List<string> args = new List<string>();
        args.Add("hoge");
        args.Add("gebu");
        args.Add("foo");
        Arguments = args.ToArray(); ;
#endif
    }
}
