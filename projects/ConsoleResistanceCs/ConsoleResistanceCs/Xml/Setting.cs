﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml.Serialization;

public class Setting : SerializeBase<Setting> {

    public const string DEFAULT_CAPTION = "%DEFAULT_CAPTION%";
    public const string DEFAULT_ICON = "%DEFAULT_ICON%";

    public class TextBoxSet {
        public string FontFamillyName { get; set; }
        public float FontEmSize { get; set; }
        public FontStyle FontStyle { get; set; }
        public byte ForeColorR { get; set; }
        public byte ForeColorG { get; set; }
        public byte ForeColorB { get; set; }
        public byte BackColorR { get; set; }
        public byte BackColorG { get; set; }
        public byte BackColorB { get; set; }

        public TextBoxSet() {
            FontFamillyName = @"ＭＳ ゴシック";
            FontEmSize = 9F;
            FontStyle = FontStyle.Regular;
            ForeColor = Color.White;
            BackColor = Color.Black;
        }

        [XmlIgnore]
        public Font Font {
            get {
                return new System.Drawing.Font(FontFamillyName, FontEmSize, FontStyle, GraphicsUnit.Point, ((byte)(128)));
            }
        }
        [XmlIgnore]
        public Color ForeColor {
            get {
                return Color.FromArgb(ForeColorR, ForeColorG, ForeColorB);
            }
            set {
                ForeColorR = value.R;
                ForeColorG = value.G;
                ForeColorB = value.B;
            }
        }
        [XmlIgnore]
        public Color BackColor {
            get {
                return Color.FromArgb(BackColorR, BackColorG, BackColorB);
            }
            set {
                BackColorR = value.R;
                BackColorG = value.G;
                BackColorB = value.B;
            }
        }
    }

    public string Caption { get; set; }
    public string IconFile { get; set; }
    public TextBoxSet Console { get; set; }
    public TextBoxSet Input { get; set; }
    public Setting() {
        Caption = DEFAULT_CAPTION;
        IconFile = DEFAULT_ICON;
        Console = new TextBoxSet();
        Input = new TextBoxSet();
#if DEBUG
        Console.BackColor = Color.FromArgb(0, 0, 64);
        Input.BackColor = Color.FromArgb(0, 0, 64);
#endif
    }
}
