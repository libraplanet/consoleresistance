﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Xml.Serialization;

public class SerializeBase<T> where T : class {
    public T Clone() {
        return (T)this.MemberwiseClone();
    }

    public static T Load(string path) {
        using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
            return Load(stream);
        }
    }

    public static T Load(Stream stream) {
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        return serializer.Deserialize(stream) as T;
    }

    public void Save(string path) {
        using (FileStream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.ReadWrite)) {
            Save(stream);
        }
    }

    public void Save(Stream stream) {
        XmlSerializer serializer = new XmlSerializer(typeof(T));
        serializer.Serialize(stream, this);
    }
}
